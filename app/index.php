<?php

require_once('vendor/autoload.php');

// Options definition: the configuration file parameter is optional
$options = \WsdlToPhp\PackageGenerator\ConfigurationReader\GeneratorOptions::instance('./wsdl-options.yml');
$options
    ->setOrigin('https://soap.telematics.tomtom.com/v1.31/objectsAndPeopleReportingService?WSDL')
    ->setDestination('./wsdl')
    ->setComposerName('tomtom/soap');
// Generator instanciation
$generator = new \WsdlToPhp\PackageGenerator\Generator\Generator($options);
// Package generation
$generator->generatePackage();
