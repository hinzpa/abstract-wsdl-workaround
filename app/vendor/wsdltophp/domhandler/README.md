# WsdlToPhp Package Generator
[![License](https://poser.pugx.org/wsdltophp/domhandler/license)](https://packagist.org/packages/wsdltophp/domhandler)
[![Latest Stable Version](https://poser.pugx.org/wsdltophp/domhandler/version.png)](https://packagist.org/packages/wsdltophp/domhandler)
[![Build Status](https://travis-ci.org/WsdlToPhp/DomHandler.svg)](https://travis-ci.org/WsdlToPhp/DomHandler)
[![PHP 7 ready](http://php7ready.timesplinter.ch/WsdlToPhp/DomHandler/badge.svg)](https://travis-ci.org/WsdlToPhp/DomHandler)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/WsdlToPhp/DomHandler/badges/quality-score.png?b=develop)](https://scrutinizer-ci.com/g/WsdlToPhp/DomHandler/?branch=develop)
[![Code Coverage](https://scrutinizer-ci.com/g/WsdlToPhp/DomHandler/badges/coverage.png?b=develop)](https://scrutinizer-ci.com/g/WsdlToPhp/DomHandler/?branch=develop)
[![StyleCI](https://styleci.io/repos/87977980/shield)](https://styleci.io/repos/87977980)

DomHandler uses the [decorator design pattern](https://en.wikipedia.org/wiki/Decorator_pattern) in order to ease DOM handling.

The source code has been originally created into the [PackageGenerator](https://github.com/WsdlToPhp/PackageGenerator) project but it felt that it had the possibility to live by itself and to evolve independtly from the PackageGenerator project if necessary.