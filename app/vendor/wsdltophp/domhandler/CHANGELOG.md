# CHANGELOG

## 1.0.0
- Initial release after exporting source code from the [PackageGenerator](https://github.com/WsdlToPhp/PackageGenerator) project
