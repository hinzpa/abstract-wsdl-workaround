ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent

build:
	docker run --rm -v /${pwd}/app://app phinz/composer:wsdl install

#############################
# Argument fix workaround
#############################
%:
	@: